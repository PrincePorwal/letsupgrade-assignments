class Book:
    def __init__(self, title, author, publication_year):
        self.title = title
        self.author = author
        self.publication_year = publication_year

    def display_info(self):
        print("Title:", self.title)
        print("Author:", self.author)
        print("Publication Year:", self.publication_year)


class Library:
    def __init__(self):
        self.books = []

    def add_book(self, book):
        self.books.append(book)

    def remove_book(self, book):
        if book in self.books:
            self.books.remove(book)
            print("Book removed successfully.")
        else:
            print("Book not found in the library.")

    def display_books(self):
        if len(self.books) == 0:
            print("The library is empty.")
        else:
            print("Books in the library:")
            for book in self.books:
                book.display_info()


# Create book objects
book1 = Book("Harry Potter and the Sorcerer's Stone", "J.K. Rowling", 1997)
book2 = Book("To Kill a Mockingbird", "Harper Lee", 1960)
book3 = Book("1984", "George Orwell", 1949)

# Create library object
library = Library()

# Add books to the library
library.add_book(book1)
library.add_book(book2)
library.add_book(book3)

# Display books in the library
library.display_books()

# Remove a book from the library
library.remove_book(book2)

# Display books again after removal
library.display_books()
