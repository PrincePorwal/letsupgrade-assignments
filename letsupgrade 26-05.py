s = '''12/01/2000
12/01/2006
12.01.2005
1/1/2004
12-01-2003
12/01/2002
01/01/2001
'''

s = s.split("\n")


for dob in s:
    if '/' in dob:
        for i in dob.split('/'):
            if len(i) < 2:
                print('0' + i, end = '')
            else:
                print(i , end = '')

    elif '-' in dob:
        for i in dob.split('-'):
            if len(i) < 2:
                print('0' + i, end = '')
            else:
                print(i , end = '')

    elif '.' in dob:
        for i in dob.split('.'):
            if len(i) < 2:
                print('0' + i, end = '')
            else:
                print(i , end = '')



