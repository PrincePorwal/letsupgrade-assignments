# Slicing a list
my_list = [1, 2, 3, 4, 5, 6]
sliced_list = my_list[2:5]  
print(sliced_list)  

my_string = "Hello, World!"

sliced_string = my_string[7:12] 


# Using range() in a for loop
for i in range(5):
    print(i) 
my_list = list(range(1, 6))  
print(my_list)  
for i in range(0, 10, 2):  
    print(i)  