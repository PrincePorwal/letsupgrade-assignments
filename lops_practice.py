# Print numbers from 1 to 5
for i in range(1, 6):
    print(i)

# Print elements of a list
fruits = ["apple", "banana", "cherry"]
for fruit in fruits:
    print(fruit)

# Calculate the sum of numbers in a list
numbers = [1, 2, 3, 4, 5]
sum = 0
for num in numbers:
    sum += num
print("Sum:", sum)

# Print numbers from 1 to 5 using a while loop
i = 1
while i <= 5:
    print(i)
    i += 1

# Print elements of a list using a while loop
fruits = ["apple", "banana", "cherry"]
index = 0
while index < len(fruits):
    print(fruits[index])
    index += 1

# Find the first even number in a list using a while loop
numbers = [1, 3, 5, 2, 4, 6]
index = 0
even_number = None
while index < len(numbers):
    if numbers[index] % 2 == 0:
        even_number = numbers[index]
        break
    index += 1
print("First even number:", even_number)
