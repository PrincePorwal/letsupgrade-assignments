""" 
s = 'hello everyone myself priyanshu porwal'
words = s.split()

hello = len(words)

for i in range(hello) :
    if i % 2 == 0:
        print(words[i][::-1], end=' ')
       
    else: print(words[i], end=' ')
 """

# Exercise 1: Print First 10 natural numbers using while loop
""" i = 0
while i < 11:
    print(i)
    i += 1
 """


# Exercise 2: Print the following pattern

""" 
for i in range(1,6):
    for j in range(1, i+1):
        print(j, end=" ")

    print("")
 """

# Exercise 3:Calculate the sum of all numbers from 1 to a given number
""" 
s = 0
number = input("Enter Number: ")
number_int = int(number)

for i in range(1, number_int + 1):
    s += i


print("sum is: ", s)
 """

# Exercise 4: Write a program to print multiplication table of a given number
""" 
number = input("wrote a number: ")

number_int = int(number)

for j in range(1,11):
    print(number_int * j)
    
 """

# Exercise 5: Display numbers from a list using loop
""" 
number = [12,75,150,180,145,525,50]

for i in number:
    if i > 500:
        break
    elif i > 150:
        continue
   
    elif i % 5 == 0:
        print(i)
    
 """