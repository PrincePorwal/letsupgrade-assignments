# Prompt the user to enter two numbers
num1 = float(input("Enter the first number: "))
num2 = float(input("Enter the second number: "))
num3 = float(input("Enter the third number: "))

greater_two = max(num1, num2)
print("The greater of the two numbers is:", greater_two)


greatest_three = max(num1, num2, num3)
print("The greatest of the three numbers is:", greatest_three)
