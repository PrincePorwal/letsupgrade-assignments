class A:
    def __init__(self, a):
        self.a = a

    def __add__(self, other):
        if isinstance(other, A):
            return A(self.a + other.a)
        else:
            return NotImplemented

a1 = A(10)
a2 = A(20)
a3 = A(40)

result = a1 + a2 + a3
print(result.a)  # Output: 70
