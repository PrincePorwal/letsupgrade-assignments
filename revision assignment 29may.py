#WAP to get first letter of the word
""" 
s = 'hi i am priyanshu'

words = [word[0] for word in s.split if len(word) >= 3] """

#WAP to display the first letter of the word along with number of characters
""" 
s = 'hi i am priyanshu porwal'
words = [[word[0], len(word)] for word in s.split() ]

print(words) """

#WAP to display the word along with number of characters
""" 
s = 'hi i am priyanshu porwal'
for word in s.split():
    print(word[::2], end = ' ')
 """

#WAP to print vowels present in a list 
s = 'hi i am priyanshu porwal'
vowels = 'aeiou'

for char in s:
    if char in vowels:
        print(char, end = ' ')
